DROP TABLE chat_daily_stats;

CREATE TABLE IF NOT EXISTS chat_daily_stats (
   interval        timestamp,
   in_total        int,
   in_uniques      hll,
   out_total       int,
   out_uniques     hll,
   PRIMARY KEY (interval)
);

INSERT INTO chat_daily_stats (interval, in_total, in_uniques) (
SELECT
    interval,
    total as in_total,
    users as in_uniques
FROM
    daily_stats
WHERE
    topic in ('meatspace.incoming')
);

UPDATE 
    chat_daily_stats 
SET
    out_total = sub.out_total,
    out_uniques = sub.out_users
FROM (
    SELECT 
        interval,
        total as out_total,
        users as out_users
    FROM
        daily_stats
    WHERE
        topic in ('meatspace.outgoing')
    ) AS sub
WHERE
    chat_daily_stats.interval = sub.interval;


