CREATE TABLE daily_stats (
    interval        timestamp,
    topic           text,
    users           hll,
    total           int,
    PRIMARY KEY (interval, topic)
);


CREATE VIEW simple_stats AS 
SELECT 
    interval, 
    topic, 
    sum(total) AS total, 
    hll_cardinality(hll_union_agg(users)) AS uniques 
FROM
    daily_stats
GROUP BY 
    interval, 
    topic
 ORDER BY 
    interval, 
    topic;
